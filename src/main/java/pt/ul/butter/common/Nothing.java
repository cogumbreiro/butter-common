package pt.ul.butter.common;

public class Nothing<T> extends Maybe<T> {
	@Override
	public boolean isNothing() {
		return true;
	}
	@Override
	public T fromJust() {
		throw new UnsupportedOperationException("Nothing");
	}
	
	@SuppressWarnings("rawtypes")
	public static final Maybe NOTHING = new Nothing();
	@SuppressWarnings("unchecked")
	public static <T> Maybe<T> nothing(T...unused) {
		return NOTHING;
	}
}