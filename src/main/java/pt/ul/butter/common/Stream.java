package pt.ul.butter.common;

public interface Stream<T> {

	/**
	 * Gets the value associated with this object.
	 * 
	 * @return The value of this stream object.
	 */
	T getValue();

	/**
	 * Gets the next stream object. It may trigger the resolve event.
	 * 
	 * @return The next stream object.
	 */
	Stream<T> getNext();

	/**
	 * Gets the next element, without resolving if needed.
	 * @return Returns null if the object was not yet resolved.
	 *
	Stream<T> getNextWithoutResolving();
	*/

	/**
	 * Adds an observer to the new stream event, triggered when the next stream
	 * is created.
	 * 
	 * @param listener
	 *            The recipient of the event.
	 * @throws NullPointerException
	 *             If the event was already triggered.
	 *
	void addNextStreamObserver(StreamListener<T> listener);
	*/
	
	/**
	 * Executes the code in <code>listener<code> when <code>getNext()<code>
	 * becomes resolved.
	 * 
	 * @param listener
	 *            The code to be executed as soon as <code>isResolved()</code>
	 *            is true.
	 *
	void lazyGetNext(StreamListener<T> listener);
	*/

	/**
	 * Checks is the next phase has already been resolved.
	 * 
	 * @return If the next object is resolved already.
	 *
	boolean isNextResolved();
	*/
}