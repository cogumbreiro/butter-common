package pt.ul.butter.common;

public abstract class Maybe<T> {
	public boolean isNothing() {
		return ! isJust();
	}
	public boolean isJust() {
		return ! isNothing();
	}
	public abstract T fromJust();
}