package pt.ul.butter.common;


/**
 * Both the value and the next stream are lazily computed.
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class LazyStream<T> implements Stream<T> {
	private static class MyFactory<T> implements Factory<Stream<T>> {
		private final Factory<? extends T> factory;
		public MyFactory(Factory<? extends T> factory) {
			this.factory = factory;
		}
		@Override
		public Stream<T> createValue() {
			return new LazyStream<>(factory);
		}
	}
	
	private final LazyValue<Stream<T>> next;
	private final T value;
	
	public LazyStream(Factory<? extends T> factory) {
		this.value = factory.createValue();
		this.next = new LazyValue<>(new MyFactory<>(factory));
	}
	
	@Override
	public T getValue() {
		return value;
	}

	@Override
	public Stream<T> getNext() {
		return next.getValue();
	}
}
