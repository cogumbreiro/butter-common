package pt.ul.butter.common;
public interface Factory<T> {
    T createValue();
}

