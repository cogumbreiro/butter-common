package pt.ul.butter.common;

import java.util.concurrent.atomic.AtomicReference;

public class LazyValue<T> {
	private final AtomicReference<T> value = new AtomicReference<>();
	private Factory<? extends T> factory;
	
	public LazyValue(Factory<? extends T> factory) {
		this.factory = factory;
	}

	public void setFactory(Factory<T> factory) {
		this.factory = factory;
	}
	
	public T getValue() {
		T result = value.get();
		if (result == null) {
			result = factory.createValue();
			if (result == null) {
				throw new IllegalStateException("Factory<T>.create() cannot return null.");
			} else if (value.compareAndSet(null, result)) {
				// onSetValue();
			} else {
				// Another thread beat us to set the value,
				// load their value instead
				result = value.get();
				assert result != null;
			}
		}
		return result;
	}
	
	public T getUnresolvedValue() {
		return value.get();
	}
	
	public boolean setIfUnresolved(T newValue) {
		return value.compareAndSet(null, newValue);
	}

	public boolean isResolved() {
		return value.get() == null;
	}
	
}
