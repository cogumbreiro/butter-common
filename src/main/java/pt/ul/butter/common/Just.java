package pt.ul.butter.common;

public class Just<T> extends Maybe<T> {
	private final T value;

	public Just(T value) {
		this.value = value;
	}
	@Override
	public boolean isJust() {
		return true;
	}
	@Override
	public T fromJust() {
		return value;
	}
}